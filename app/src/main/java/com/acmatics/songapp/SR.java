package com.acmatics.songapp;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.acmatics.songapp.common.URLConstants;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 5/26/2015.
 */
public class SR extends Fragment {

    View rootView;
    ImageButton karaokePlay;
    ImageButton karaokeStop;
    ImageButton songPlay;
    //    private MediaPlayer mediaPlayer;
    int length;
    int songLength;
    //    private MediaPlayer mediaPlayerSong;
    boolean isPlay;
    TextView songName;
    private AQuery aq;

    public SR() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_sr, container, false);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        aq = new AQuery(getActivity());
        SongsSelection.mediaPlayerSong = new MediaPlayer();
        try {
            SongsSelection.mediaPlayerSong.setDataSource(getActivity().openFileInput("test.mp3").getFD());
            SongsSelection.mediaPlayerSong.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        songName = (TextView) rootView.findViewById(R.id.song_name);

        karaokePlay = (ImageButton) rootView.findViewById(R.id.karaoke_play);
        karaokePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPlay == false) {
                    if (length == 0) {
                        SongsSelection.mediaPlayer = MediaPlayer.create(getActivity(), R.raw.test1);
                        SongsSelection.mediaPlayer.start();
                        karaokePlay.setImageResource(R.drawable.ic_action_pause);
                        isPlay = true;
                    } else {
                        SongsSelection.mediaPlayer.seekTo(length);
                        SongsSelection.mediaPlayer.start();
                        karaokePlay.setImageResource(R.drawable.ic_action_pause);
                        isPlay = true;
                    }
                } else {
                    SongsSelection.mediaPlayer.pause();
                    karaokePlay.setImageResource(R.drawable.ic_action_play);
                    length = SongsSelection.mediaPlayer.getCurrentPosition();
                    isPlay = false;
                }
            }
        });
        karaokeStop = (ImageButton) rootView.findViewById(R.id.karaoke_stop);
        karaokeStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPlay == true) {
                    SongsSelection.mediaPlayer.stop();
                    karaokePlay.setImageResource(R.drawable.ic_action_play);
                    length = 0;
                    isPlay = false;
                }
            }
        });

//        SongsSelection.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mediaPlayer) {
//                karaokePlay.setImageResource(R.drawable.ic_action_play);
//                isPlay = false;
//            }
//        });

        songPlay = (ImageButton) rootView.findViewById(R.id.song_play);
        songPlay.setEnabled(false);
        Button submitKey = (Button) rootView.findViewById(R.id.key_submit);
        submitKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String audioTrackKey = ((EditText) rootView.findViewById(R.id.keyEditText)).getText().toString();
                if (audioTrackKey.trim().equals("")) {
                    ((TextView) rootView.findViewById(R.id.audioTrackKeyError)).setVisibility(View.VISIBLE);
                    ((TextView) rootView.findViewById(R.id.audioTrackKeyError)).setText("Please enter a key");
                } else {
                    ((TextView) rootView.findViewById(R.id.audioTrackKeyError)).setVisibility(View.GONE);
                    JSONObject json = new JSONObject();
                    try {
                        json.put("audioTrackKey", audioTrackKey);
                        json.put("audioTrackType", "SR");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ProgressDialog progress = new ProgressDialog(getActivity());
                    progress.setMessage("Validating Key...");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.setCancelable(false);
                    String url = URLConstants.BASE_URL + URLConstants.VALIDATE_AUDIO_TRACK_KEY;
                    aq.progress(progress).post(url, json, JSONObject.class, new AjaxCallback<JSONObject>() {
                        @Override
                        public void callback(String url, JSONObject json, AjaxStatus status) {
                            if (json != null) {
                                try {
                                    if (json.has("validKey") && json.getBoolean("validKey")) {
                                        ((TextView) rootView.findViewById(R.id.audioTrackKeyError)).setVisibility(View.GONE);
                                        songPlay.setEnabled(true);
                                        songPlay.setBackgroundResource(R.drawable.shape_bg);
                                    } else {
                                        ((TextView) rootView.findViewById(R.id.audioTrackKeyError)).setVisibility(View.VISIBLE);
                                        ((TextView) rootView.findViewById(R.id.audioTrackKeyError)).setText("Key in not valid");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    });
                }
            }
        });
        songPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SongsSelection.isPlaying == true) {
                    if(songLength==0) {
//                    SongsSelection.mediaPlayerSong = MediaPlayer.create(getActivity(), R.raw.london);
                        SongsSelection.mediaPlayerSong.start();
//                        songPlay.setEnabled(false);
                        songPlay.setImageResource(R.drawable.ic_action_pause);
                        SongsSelection.isPlaying = false;
                    }
                    else{
                        SongsSelection.mediaPlayerSong.seekTo(songLength);
                        SongsSelection.mediaPlayerSong.start();
                        songPlay.setImageResource(R.drawable.ic_action_pause);
                        SongsSelection.isPlaying = false;
                    }
                }
                else{
                    SongsSelection.mediaPlayerSong.pause();
                    songPlay.setImageResource(R.drawable.ic_action_play);
                    songLength = SongsSelection.mediaPlayerSong.getCurrentPosition();
                    SongsSelection.isPlaying = true;
                }
//                else{
//                    Toast.makeText(getActivity(),"Please ",Toast.LENGTH_SHORT).show();
//                }
            }
        });
        SongsSelection.mediaPlayerSong.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                SongsSelection.isPlaying = true;
                songPlay.setEnabled(false);
                songPlay.setBackgroundResource(R.drawable.shape_bg_grey);
            }
        });
    }
}
