package com.acmatics.songapp.common;

public interface URLConstants {
    public static final String BASE_URL =  "http://ec2-54-69-216-189.us-west-2.compute.amazonaws.com/brainzx/";
    public static final String VALIDATE_AUDIO_TRACK_KEY = "validateAudioTrackKey";
}
