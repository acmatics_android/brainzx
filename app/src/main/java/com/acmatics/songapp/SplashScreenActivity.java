package com.acmatics.songapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ProgressBar;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


public class SplashScreenActivity extends AppCompatActivity {
    private static final String PREFS_NAME = "Brainzx";

    private static char[] flleName = com.acmatics.songapp.android.File.fileName();

    private ZipFile zFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        ((ProgressBar) findViewById(R.id.progress)).getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        Thread startThread = new Thread() {
            @Override
            public void run() {
                // Restore preferences
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                boolean isFirstRun = settings.getBoolean("isFirstRun", true);

                if (isFirstRun) {
                    decodeFile(R.raw.lon, "london.mp3");
                    decodeFile(R.raw.pat, "patiala.mp3");
                    decodeFile(R.raw.bir, "birthday.mp3");
                    decodeFile(R.raw.tes, "test.mp3");

                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("isFirstRun", false);
                    editor.commit();
                } else {
                    try {
                        sleep(3000l);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Intent i = new Intent(SplashScreenActivity.this, SongsSelection.class);
                startActivity(i);
                finish();
            }
        };
        startThread.start();
    }

    private String path = Environment.getExternalStorageDirectory() + "/temp";

    private void decodeFile(int id, String fileName) {
        try {
            InputStream is = getResources().openRawResource(id);

            String tempDest = Environment.getExternalStorageDirectory() + "/temp.zip";
            File tempZip = new File(tempDest);
            FileOutputStream os = new FileOutputStream(tempZip);
            copyFile(is, os);

            zFile = new ZipFile(tempZip);
            zFile.setFileNameCharset("GBK");
            if (!zFile.isValidZipFile()) {
                throw new ZipException(",.");
            }
            File tempDir = new File(path);
            if (tempDir.isDirectory() && !tempDir.exists()) {
                tempDir.mkdir();
            }

            decipherFile(fileName);

            zFile.extractAll(path);

            tempZip.delete();

            File tempFile = new File(path + "/" + fileName);
            FileInputStream input = new FileInputStream(tempFile);
            FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);

            copyFile(input, fos);

            tempFile.delete();
            tempDir.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void copyFile(InputStream is, FileOutputStream os) throws IOException {
        byte[] buffer = new byte[256];
        int bytesRead = 0;
        while ((bytesRead = is.read(buffer)) != -1) {
            os.write(buffer, 0, bytesRead);
        }
        is.close();
        os.close();
    }

    private void decipherFile(String fileName) throws ZipException {
        if (zFile.isEncrypted()) {
            zFile.setPassword(flleName);
        }
    }
}
